# How to create a teaser/poster for CHM

## Intro

Teasers (or posters) are used on social platforms and on the official website to advertise CHM events.

Their role is to tell the viewer, in a glance, essential information about the event.

Although a teaser can be created with any graphical tool, following this procedure guarantees extremely fast and error-proof results.

Generating a new teaser will take between 10 and 20 minutes depending on your personal speed.

## Requirements


### Dimensions

Teasers require standard dimensions otherwise they will break the website layout.

Creating a new teaser must produce, as output, one 1980x1080 pixels picture in PNG format.

Also, some social media require a squared layout. For that case we have a 1400x1400 pixel template.

### Nomenclature

Later on the produced teaser will be subject to additional automatic manipulations so its filename must follow this standard nomenclature: `YYYY-mm-dd_teaser_1920x1080.png`

Ex.: `2024-03-09_teaser_1920x1080.png` which means: _"teaser for March 9th 2024 with size 1920x1080"

## Tools

You can generate a teaser with any vectorial graphic tool but Inkscape is highly adviced.

## Expected result sample

This is how a teaser should look like:

![sample teaser](_procedure_files/teaser_sample_06_1920x1080.png)

You can see more examples in `procedures/teaser-poster/teaser_sample_output`

## Steps by step guide

### Step 1

Obtain or generate (maybe with AI) a background picture that you like but relevant to the event topic. Save it anywhere in your pc.

The background picture can be in any format but make sure that its size is 1920x1080. Use [GIMP](https://www.gimp.org/) or any other graphic software to resize/adjust the picture.

**WARNING!** If you download a photo from the internet, double check the licensing for it and **make sure** that using it won't infringe any copyright law. [Google Images](https://images.google.com/) has a specific "licencing tool" designed for this purpose.


### Step 2

This step is required only the first time otherwise move to step 3.

Install in your system the fonts listed in `components/fonts/`. In linux, it's enough to copy the files in `${HOME}/.local/share/fonts`.

### Step 3

This step is required only the first time otherwise move to step 4.

If you don't know how to open a terminal or how to use `git` ask damko, feres or mj to give you the teaser template file otherwise open a terminal and run:

```
git clone https://gitlab.com/crypto-hub-malta/procedures.git
```

### Step 4

If you pulled the `procedures repo` from gitlab then run:

```
cd <your_path>/procedures
git pull
```

### Step 5

Open with [inkscape](https://inkscape.org/) (or any other vector graphic software) the template file found in this repository `teaser_template.svg`

Ex.:

```
 inkscape /home/user/projects/shared/cryptohub/procedures/teaser-poster/teaser_template.svg
```

Open `teaser_template_squared.svg` in case you need a squared format.

This is what you see the first time you open the template:

![sample teaser](_procedure_files/teaser_sample_no_background_1920x1080.png)


Select and unlock the `event_background` layer.

![select background layer](_procedure_files/inkscape_select_background_layer.png)

Drag and drop the background picture you obtained in step 1.

If you have difficulties importing the picture, check this [guide](https://inkscape-manuals.readthedocs.io/en/latest/import-pictures.html)

Stretch/adjust the picture to fit in the frame.

Lock the `event_background` layer.

### Step 6

Select and unlock the `event_date` layer.

![select date layer](_procedure_files/inkscape_select_date_layer.png)

Click on the date and change it. No further change should be required in this layer.

Lock the `event_date` layer.

### Step 7

Select and unlock the `event_title` layer.

![select title layer](_procedure_files/inkscape_select_title_layer.png)

Click on the title and change it.

Most likely you will have to change the size of the underlaying blurred dark background as the title varies a lot in length from event to event.

Finally center the title and its blurred background with the date above.

Lock the `event_title` layer.

### Step 8

Export the teaser file with the right nomenclature.

![export](_procedure_files/inkscape_export.png)

### Step 9

Exit inkscape **without saving**.

### Step 10

Send the teaser file to feres, damko or mj


## QRcodes

If you need to add qrcodes, you can generate them (obviously in the same style as in the current template) from [here](https://qrplanet.com/designer-qr-code-with-embeded-logo#link) . The style used is the basic one with the addition of an icon when needed (to clarify the context).

